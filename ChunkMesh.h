#ifndef BGFX_TEST_CHUNKMESH_H
#define BGFX_TEST_CHUNKMESH_H

//namespace VoxelGame {

#include <bx/uint32_t.h>
#include "bgfx_utils.h"

#include "VoxelDecl.h"

struct PosNormalColorVertex
{
    float m_pos[3];
    float m_normal[3];
    uint8_t m_abgr[4];

    static void init()
    {
        ms_decl
                .begin()
                .add(bgfx::Attrib::Position, 3, bgfx::AttribType::Float)
                .add(bgfx::Attrib::Normal,   3, bgfx::AttribType::Float)
                .add(bgfx::Attrib::Color0,   4, bgfx::AttribType::Uint8, true)
                .end();
    };

    static bgfx::VertexDecl ms_decl;
};


struct ChunkMesh {
    bgfx::VertexBufferHandle m_vbh;
    bgfx::IndexBufferHandle m_ibh;
    PolyVox::Vector3DFloat m_origin;
    bool _dirty;
public:
    ChunkMesh();
    ~ChunkMesh();

    void update();

    void reset();
    void draw(bgfx::ProgramHandle program);

    std::vector<PosNormalColorVertex> m_vertex;
    std::vector<uint16_t> m_indices;
};


//}

#endif //BGFX_TEST_CHUNKMESH_H
