//
// Created by vugluskr on 03.08.16.
//

#ifndef BGFX_TEST_COLORSABGR_H
#define BGFX_TEST_COLORSABGR_H

#include "common.h"

namespace ColorsABGR
{
    const uint32_t TRANSPARENT_COLOR = 0x00000000;

    const uint32_t RED = 0xFF0000FF;
    const uint32_t GREEN = 0xFF00FF00;
    const uint32_t BLUE = 0xFFFF0000;

    const uint32_t GRAY = 0xFF444444;

    const uint32_t MAGENTA = 0xFFFF00FF;
};


#endif //BGFX_TEST_COLORSABGR_H
