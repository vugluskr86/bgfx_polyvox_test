cmake_minimum_required(VERSION 3.5)
project(bgfx_test)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14") #

# find_library(BULLET_LIBRARY NAMES bullet)

add_subdirectory(common/libs/ib-compress)
add_subdirectory(common/libs/lodepng)
add_subdirectory(common/libs/ocornut-imgui)
add_subdirectory(common/libs/remotery)
add_subdirectory(common/libs/stb)

add_subdirectory(common)

# FIXME : bullet library
include_directories(common common/imgui common/libs /usr/local/include/bullet)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/runtime)

set(SOURCE_FILES VoxelWorldRender.cpp Perlin.cpp cubes.cpp VoxelDecl.cpp ChunkMesh.cpp Player.cpp Player.h GameState.h GameScene.cpp GameScene.h WorldGameScene.cpp WorldGameScene.h GameActor.cpp GameActor.h)

add_executable(bgfx_test ${SOURCE_FILES})

target_link_libraries (bgfx_test bgfxDebug common ib-compress ocornut-imgui remotery stb pthread dl X11 GL)

add_custom_command(TARGET bgfx_test
    PRE_BUILD
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/assets/shaders

    COMMAND ${PROJECT_SOURCE_DIR}/bin/shaderc -i ${PROJECT_SOURCE_DIR}/common -f ${PROJECT_SOURCE_DIR}/assets/shaders/vs_cubes.sc -o ${PROJECT_SOURCE_DIR}/runtime/shaders/glsl/vs_cubes.bin --type vertex --platform linux -p 120
    COMMAND ${PROJECT_SOURCE_DIR}/bin/shaderc -i ${PROJECT_SOURCE_DIR}/common -f ${PROJECT_SOURCE_DIR}/assets/shaders/fs_cubes.sc -o ${PROJECT_SOURCE_DIR}/runtime/shaders/glsl/fs_cubes.bin --type fragment --platform linux -p 120

    COMMAND ${PROJECT_SOURCE_DIR}/bin/shaderc -i ${PROJECT_SOURCE_DIR}/common -f ${PROJECT_SOURCE_DIR}/assets/shaders/mesh/vs_mesh.sc -o ${PROJECT_SOURCE_DIR}/runtime/shaders/glsl/vs_mesh.bin --type vertex --platform linux -p 120
    COMMAND ${PROJECT_SOURCE_DIR}/bin/shaderc -i ${PROJECT_SOURCE_DIR}/common -f ${PROJECT_SOURCE_DIR}/assets/shaders/mesh/fs_mesh.sc -o ${PROJECT_SOURCE_DIR}/runtime/shaders/glsl/fs_mesh.bin --type fragment --platform linux -p 120

    COMMENT "Compile shader resource"
)