//
// Created by vugluskr on 03.08.16.
//

#ifndef BGFX_TEST_VOXELDECL_H
#define BGFX_TEST_VOXELDECL_H

#include "ColorsABGR.h"

#include "PolyVox/RawVolume.h"
#include "PolyVox/MaterialDensityPair.h"
#include "PolyVox/PagedVolume.h"
#include "PolyVox/Region.h"
#include "PolyVox/Picking.h"
#include "PolyVox/AmbientOcclusionCalculator.h"
#include "PolyVox/CubicSurfaceExtractor.h"
#include "PolyVox/MarchingCubesSurfaceExtractor.h"
#include "PolyVox/Array.h"
#include "PolyVox/BaseVolume.h"
#include "PolyVox/DefaultIsQuadNeeded.h"
#include "PolyVox/Mesh.h"
#include "PolyVox/Vertex.h"
#include "PolyVox/AmbientOcclusionCalculator.h"

//namespace VoxelGame {

/*
struct VoxelDecl {
    uint32_t m_abgr;
    uint8_t m_type;

    uint8_t m_ao[4];

    VoxelDecl() {

        // m_ao = { 0, 0, 0, 0, 0, 0, 0, 0 };

        memset(&m_ao[0], 0, sizeof(uint8_t) * 4);

        m_type = 0;
        m_abgr = ColorsABGR::TRANSPARENT_COLOR;
    }

    VoxelDecl(uint8_t type, uint32_t abgr) :
            m_type(type)
            , m_abgr(abgr) {

        memset(&m_ao[0], 0, sizeof(uint8_t) * 4);

        //  m_ao = { 0, 0, 0, 0, 0, 0, 0, 0 };
    }

    VoxelDecl& operator=(const VoxelDecl& right) {
        if (this == &right) {
            return *this;
        }

        m_type    = right.m_type;
        m_abgr    = right.m_abgr;

        memcpy(&m_ao[0], &right.m_ao[0], sizeof(uint8_t) * 4);

        return *this;
    }

    inline bool isTransparent() {
        uint8_t a[4];

        a[0] = m_abgr >> 24;
        a[1] = m_abgr >> 16;
        a[2] = m_abgr >>  8;
        a[3] = m_abgr;

        return a[0] == 0x00;
    }
};

bool operator==(const VoxelDecl& left, const VoxelDecl& right);
bool operator!=(const VoxelDecl& left, const VoxelDecl& right);

namespace VoxelTypes {
    const VoxelDecl AIR(0,   ColorsABGR::TRANSPARENT_COLOR);
    const VoxelDecl GRASS(1, ColorsABGR::GREEN);
    const VoxelDecl WATER(2, ColorsABGR::BLUE);
    const VoxelDecl STONE(3, ColorsABGR::GRAY);
};
*/

typedef PolyVox::MaterialDensityPair44 VoxelDecl;
typedef PolyVox::PagedVolume<VoxelDecl> VoxelVolume;

struct ABGR {

    union {
        uint8_t components[4];
        struct {
            uint8_t blue,green,red,alpha;
        };
        uint32_t raw;
    };

    uint8_t &operator[](std::size_t index){
        return components[4 - index];
    }

    ABGR (uint32_t raw_):raw(raw_){}
    ABGR (uint8_t r, uint8_t g, uint8_t b, uint8_t a):
            red(r), green(g), blue(b),alpha(a){}
};

/*
PolyVox::Vertex<uint32_t> decodeVoxelVertex(const PolyVox::CubicVertex<VoxelDecl>& cubicVertex);

template <typename MeshType>
PolyVox::Mesh< PolyVox::Vertex<uint32_t>, uint16_t> decodeVoxelMesh(const MeshType& encodedMesh)
{
    PolyVox::Mesh< PolyVox::Vertex<uint32_t>, uint16_t> decodedMesh;

    for (typename MeshType::IndexType ct = 0; ct < encodedMesh.getNoOfVertices(); ct++)
    {
        decodedMesh.addVertex(decodeVoxelVertex(encodedMesh.getVertex(ct)));
    }

    POLYVOX_ASSERT(encodedMesh.getNoOfIndices() % 3 == 0, "The number of indices must always be a multiple of three.");
    for (uint32_t ct = 0; ct < encodedMesh.getNoOfIndices(); ct += 3)
    {
        decodedMesh.addTriangle(encodedMesh.getIndex(ct), encodedMesh.getIndex(ct + 1), encodedMesh.getIndex(ct + 2));
    }

    decodedMesh.setOffset(encodedMesh.getOffset());

    return decodedMesh;
}
*/

/*
class VoxelDeclIsQuadNeeded
{
public:
    bool operator()(VoxelDecl back, VoxelDecl front, VoxelDecl& materialToUse)
    {
        if (!back.isTransparent() && front.isTransparent())
        {
            materialToUse = static_cast<VoxelDecl>(back);
            return true;
        }
        else
        {
            return false;
        }
    }
};
*/

bool isVoxelTransparentFunction(VoxelDecl voxel);
void updateAmbient(PolyVox::RawVolume<VoxelDecl>* volData, const PolyVox::Region& region);

//}

#endif //BGFX_TEST_VOXELDECL_H
