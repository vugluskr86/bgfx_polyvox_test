//
// Created by vugluskr on 28.08.16.
//

#ifndef BGFX_TEST_GAMESTATE_H
#define BGFX_TEST_GAMESTATE_H

enum eGameState {
    STATE_MENU = 0,
    STATE_GAME_WORLD_SCENE
};

#endif //BGFX_TEST_GAMESTATE_H
