#include "VoxelWorldRender.h"

#include <cmath>
#include <limits>
#include <vector>
#include <algorithm>
#include <iterator>


#include "debugdraw/debugdraw.h"


//namespace VoxelGame {

VoxelWorldRender::VoxelWorldRender(VoxelVolume *volumeData) :
        mPagingDistanceX(2)
      , mPagingDistanceY(2)
      , mPagingDistanceZ(2)
      , _init(false)
      , shutdownThreads(false)
      , mVolumeData(volumeData)
//      , _chunkView(mChunkSizeX, mChunkSizeY, mChunkSizeZ)
{
    int infinity = std::numeric_limits<int>::max();
    _lastChunkPos = PolyVox::Vector3DInt32(infinity, infinity, infinity);


    //PerlinNoisePager* pager = new PerlinNoisePager();
  //  mVolumeData = new PolyVox::PagedVolume<VoxelDecl>(pager, 128 * 1024 * 1024, 64);


    _chunkAllocDebug = 0;

}

VoxelWorldRender::~VoxelWorldRender()
{
    shutdownThreads = true;
    for (auto& thread : mThreads) {
        thread.join();
    }
}

void VoxelWorldRender::start() {
    mThreads.push_back(
        std::thread(&VoxelWorldRender::extractSurface, this)
    );
    for (auto& thread : mThreads) {
        thread.detach();
    }
}

void VoxelWorldRender::enqueueSurfaceExtraction(PolyVox::Region region) {
    mSurfaceExtractionQueue.push( region );

    // Wake consumer.
    extractionQueueNotEmpty.notify_one();
}

void VoxelWorldRender::extractSurface() {
    while (not shutdownThreads) {

        std::unique_lock<std::mutex> s(mSurfaceExtractionQueueMutex);

        if (not mSurfaceExtractionQueue.empty()) {
            //std::lock_guard<std::recursive_mutex> guard(mutex);

            clock_t begin = clock();

            mSurfaceExtractionMutex.lock();

            PolyVox::Region region = mSurfaceExtractionQueue.front();

            auto encodedMesh = PolyVox::extractCubicMesh(mVolumeData, region);
            auto mesh = decodeMesh(encodedMesh);


            if( mesh.getNoOfVertices() > 0 ) {

                ChunkMesh* man_obj = getChunkMesh();

                man_obj->m_origin = static_cast<PolyVox::Vector3DFloat>(mesh.getOffset());

                man_obj->m_indices.empty();
                man_obj->m_vertex.empty();

                man_obj->m_indices.clear();
                man_obj->m_vertex.clear();

                for(size_t i = 0; i < mesh.getNoOfIndices(); i++) {
                    man_obj->m_indices.push_back( mesh.getIndex(i) );
                }

                PosNormalColorVertex vertex;

                for(size_t i = 0; i < mesh.getNoOfVertices(); i++) {
                    auto v = mesh.getVertex(i);

                    vertex.m_pos[0] = v.position.getX();
                    vertex.m_pos[1] = v.position.getY();
                    vertex.m_pos[2] = v.position.getZ();

                    vertex.m_normal[0] = v.normal.getX();
                    vertex.m_normal[1] = v.normal.getY();
                    vertex.m_normal[2] = v.normal.getZ();

                    vertex.m_abgr[0] = 255;
                    vertex.m_abgr[1] = 0;
                    vertex.m_abgr[2] = 0;
                    vertex.m_abgr[3] = 255;

                    man_obj->m_vertex.push_back(vertex);
                }

                mSurfaceRenderQueue.push(man_obj);
            }
            mSurfaceExtractionQueue.pop();
            mSurfaceExtractionMutex.unlock();

            clock_t end = clock();
            double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

            std::cout << "Chunk generated: " << elapsed_secs << std::endl;
        }
        // Sleep until woken by producer.
        extractionQueueNotEmpty.wait(
            s,
            [this]() {
                return not mSurfaceExtractionQueue.empty();
            }
        );
    }
}

void VoxelWorldRender::_fillChunkView(const PolyVox::Vector3DInt32& chunkPos, std::vector<PolyVox::Vector3DInt32> & chunks)
{
    for(int32_t x = -mPagingDistanceX; x <= mPagingDistanceX; x++) {
        for(int32_t y = -mPagingDistanceY; y <= mPagingDistanceY; y++) {
            for(int32_t z = -mPagingDistanceZ; z <= mPagingDistanceZ; z++) {
                chunks.emplace_back(PolyVox::Vector3DInt32(chunkPos.getX() + x, chunkPos.getY() + y, chunkPos.getZ() + z));
            }
        }
    }
}

PolyVox::Region VoxelWorldRender::_makeChunkViewRegion(const PolyVox::Vector3DInt32 &chunkPos) {
    return PolyVox::Region(
            PolyVox::Vector3DInt32(
                chunkPos.getX() - mPagingDistanceY,
                chunkPos.getY() - mPagingDistanceX,
                chunkPos.getZ() - mPagingDistanceZ
            ),
            PolyVox::Vector3DInt32(
                chunkPos.getX() + mPagingDistanceY,
                chunkPos.getY() + mPagingDistanceX,
                chunkPos.getZ() + mPagingDistanceZ
            ));
}

PolyVox::Vector3DInt32 VoxelWorldRender::Voxel2Chunk(const PolyVox::Vector3DInt32& eue) {
    return PolyVox::Vector3DInt32(
        eue.getX() / mChunkSizeX,
        eue.getY() / mChunkSizeY,
        eue.getZ() / mChunkSizeZ
    );
}

bool VoxelWorldRender::ChunkExist(const PolyVox::Vector3DInt32& chunkPos) {
    for(auto c : mChunks) {
        PolyVox::Vector3DInt32 chunk(
                c->m_origin.getX() / mChunkSizeX,
                c->m_origin.getY() / mChunkSizeY,
                c->m_origin.getZ() / mChunkSizeZ
        );

        if( chunk == chunkPos ) {
            return true;
        }
    }

    return false;
}

/*
private IEnumerator UpdateChunks(){
    for (int i = 1; i < VIEW_RANGE; i += ChunkWidth) {
        float vr = i;
        for (float x = transform.position.x - vr; x < transform.position.x + vr; x += ChunkWidth) {
            for (float z = transform.position.z - vr; z < transform.position.z + vr; z += ChunkWidth) {
                _pos.Set(x, 0, z); // no y, yet
                _pos.x = Mathf.Floor(_pos.x/ChunkWidth)*ChunkWidth;
                _pos.z = Mathf.Floor(_pos.z/ChunkWidth)*ChunkWidth;
                Chunk chunk = Chunk.FindChunk(_pos);

                // If Chunk is already created, continue
                if (chunk != null)
                    continue;
                // Create a new Chunk..
                chunk = (Chunk) Instantiate(ChunkFab, _pos, Quaternion.identity);
            }
        }
        // Skip to next frame
        yield return 0;
    }
}

https://github.com/cuberite/cuberite/blob/master/src/ClientHandle.cpp#L418

*/

void VoxelWorldRender::update(const PolyVox::Vector3DInt32& pos)
{
    PolyVox::Vector3DInt32 curChunkPos = Voxel2Chunk(pos);
    PolyVox::Region curRegion = _makeChunkViewRegion(curChunkPos);

    while (not mSurfaceRenderQueue.empty()) {

        mSurfaceRenderMutex.lock();

        ChunkMesh* man_obj = mSurfaceRenderQueue.front();

        PolyVox::Vector3DInt32 chunk(
                (man_obj->m_origin.getX() + mChunkSizeX / 2.0f)  / mChunkSizeX,
                (man_obj->m_origin.getY() + mChunkSizeY / 2.0f)  / mChunkSizeY,
                (man_obj->m_origin.getZ() + mChunkSizeZ / 2.0f)  / mChunkSizeZ
        );

        bool exist = curRegion.containsPoint(chunk);

        if( exist ) {
            man_obj->update();
        }

        mSurfaceRenderQueue.pop();

        if( exist ) {
            mChunks.push_back(man_obj);
        } else {
            returnChunkMesh(man_obj);
        }

        mSurfaceRenderMutex.unlock();
    }

    if (curChunkPos == _lastChunkPos) {
        return;
    }

    _visibleChunks.clear();


    _fillChunkView(curChunkPos, _visibleChunks);

    _lastChunks.clear();
    if(_init) {
        _fillChunkView(_lastChunkPos, _lastChunks);
    } else {
        _fillChunkView(curChunkPos, _lastChunks);
        _init = true;
    }

    mChunks.remove_if([this, curRegion](ChunkMesh* mesh) {
        PolyVox::Vector3DInt32 chunk(
                (mesh->m_origin.getX() + mChunkSizeX / 2.0f)  / mChunkSizeX,
                (mesh->m_origin.getY() + mChunkSizeY / 2.0f)  / mChunkSizeY,
                (mesh->m_origin.getZ() + mChunkSizeZ / 2.0f)  / mChunkSizeZ
        );

        bool remove = !curRegion.containsPoint(chunk);

        if( remove ) {
            mesh->reset();
            returnChunkMesh(mesh);
        }

        return remove;
    });

    for(auto c : _visibleChunks) {
        if( !ChunkExist(c) ) {
            PolyVox::Vector3DInt32 upper(
                    c.getX() * mChunkSizeX + mChunkSizeX / 2,
                    c.getY() * mChunkSizeY + mChunkSizeY / 2,
                    c.getZ() * mChunkSizeZ + mChunkSizeZ / 2
            );

            PolyVox::Vector3DInt32 lower(
                    c.getX() * mChunkSizeX - mChunkSizeX / 2,
                    c.getY() * mChunkSizeY - mChunkSizeY / 2,
                    c.getZ() * mChunkSizeZ - mChunkSizeZ / 2
            );

            PolyVox::Region region(lower, upper);

            enqueueSurfaceExtraction(region);
        }
    }

    _lastChunkPos = curChunkPos;
}

void VoxelWorldRender::render(bgfx::ProgramHandle program) {
    for(auto c : mChunks) {
        c->draw(program);
    }
}


void VoxelWorldRender::renderInfo(uint16_t startLine) {

    bgfx::dbgTextPrintf(0, startLine + 0, 0x4f, "VisibleChunks: %d", _visibleChunks.size());
    bgfx::dbgTextPrintf(0, startLine + 1, 0x4f, "LastChunks: %d", _lastChunks.size());
    bgfx::dbgTextPrintf(0, startLine + 2, 0x4f, "Chunks: %d", mChunks.size());


}

void VoxelWorldRender::renderDebug()
{
    for(auto c : mChunks) {

     //   ddDrawAxis(c->m_origin.getX(), c->m_origin.getY(), c->m_origin.getZ(), 100.0f, Axis::Count, 1.4f);

        Aabb chunkAABB;

        chunkAABB.m_min[0] = c->m_origin.getX();
        chunkAABB.m_min[1] = c->m_origin.getY();
        chunkAABB.m_min[2] = c->m_origin.getZ();

        chunkAABB.m_max[0] = c->m_origin.getX() + mChunkSizeX;
        chunkAABB.m_max[1] = c->m_origin.getY() + mChunkSizeY;
        chunkAABB.m_max[2] = c->m_origin.getZ() + mChunkSizeZ;

        ddDraw(chunkAABB);
    }
}

//}
