//
// Created by vugluskr on 20.08.16.
//

#ifndef BGFX_TEST_VOXELWORLD_H
#define BGFX_TEST_VOXELWORLD_H


//#include <stack>
#include <queue>
#include <vector>
#include <tuple>

// C++11 libraries
#include <condition_variable>
#include <mutex>
#include <thread>

#include "ChunkMesh.h"

//namespace VoxelGame {

// TODO :
// Список чанков
// Сотояние для чанка
// Блокировка на список
//

class VoxelWorldRender {
public:

    VoxelWorldRender(VoxelVolume *volumeData);

    virtual ~VoxelWorldRender();

    virtual void enqueueSurfaceExtraction(PolyVox::Region region);

    virtual void extractSurface();

    void update(const PolyVox::Vector3DInt32& pos);
    void render(bgfx::ProgramHandle program);

    void start();

    PolyVox::Vector3DInt32 Voxel2Chunk(const PolyVox::Vector3DInt32& eue);
    void renderInfo(uint16_t startLine);

    void renderDebug();


protected:
    VoxelVolume *mVolumeData;
private:
    std::condition_variable extractionQueueNotEmpty;
    bool shutdownThreads;

    std::vector<std::thread> mThreads;
    std::mutex mSurfaceExtractionMutex;
    std::mutex mSurfaceExtractionQueueMutex;
    std::mutex mSurfaceRenderMutex;
    std::queue<PolyVox::Region> mSurfaceExtractionQueue;
    std::queue<ChunkMesh*> mSurfaceRenderQueue;

    static const bool DEBUG = true;


    // statistic
    std::vector<PolyVox::Vector3DInt32> _visibleChunks;
    std::vector<PolyVox::Vector3DInt32> _lastChunks;

    std::vector<ChunkMesh*> _removedChunks;


    PolyVox::Vector3DInt32 _lastChunkPos;

    int32_t mPagingDistanceX;
    int32_t mPagingDistanceY;
    int32_t mPagingDistanceZ;

    static const int mChunkSizeX = 32;
    static const int mChunkSizeY = 32;
    static const int mChunkSizeZ = 32;

    std::list<ChunkMesh*> _chunkMeshPool;
    ChunkMesh* getChunkMesh()
    {
        if (_chunkMeshPool.empty())
        {
            std::cout << "Create new chunk."  << _chunkAllocDebug << std::endl;
            _chunkAllocDebug++;
            return new ChunkMesh;
        }
        else
        {
            std::cout << "Reusing existing chunk." << _chunkMeshPool.size() << std::endl;
            ChunkMesh* r = _chunkMeshPool.front();
            _chunkMeshPool.pop_front();
            return r;
        }
    }
    void returnChunkMesh(ChunkMesh* object)
    {
        object->reset();
        _chunkMeshPool.push_back(object);
    }

    bool ChunkExist(const PolyVox::Vector3DInt32& chunkPos);

    int32_t _chunkAllocDebug;

    std::list<ChunkMesh*> mChunks;

    bool _init;

    void _fillChunkView(const PolyVox::Vector3DInt32& chunkPos, std::vector<PolyVox::Vector3DInt32> & chunks);

    PolyVox::Region _makeChunkViewRegion(const PolyVox::Vector3DInt32 &chunkPos);
};

//}

#endif //BGFX_TEST_VOXELWORLD_H
