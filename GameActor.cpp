//
// Created by vugluskr on 28.08.16.
//

#include "GameActor.h"

#include "common.h"
#include "debugdraw/debugdraw.h"

GameActor::GameActor(ActorState &state)
{
    _state = state;
    _dy = 0;
    _verticalAngle = _horizontalAngle = 0.0f;
    _moveSpeed = 0.0f;
    _mesh = meshLoad("meshes/bunny.bin");
}

GameActor::~GameActor()
{
    // TODO
}

// input
void GameActor::actionMoveForward()
{
    _moveSpeed = _state._flying ? _state._flySpeed : _state._moveSpeed;
}

void GameActor::actionMoveBackward()
{
    _moveSpeed = -(_state._flying ? _state._flySpeed : _state._moveSpeed);
}

void GameActor::actionMoveStop()
{
    _moveSpeed = 0.0f;
}


void GameActor::actionRotateY(float angle)
{
    _horizontalAngle += angle;

    float m[16];
    bx::mtxIdentity(m);
    bx::mtxRotateY(m, angle);


    float rot[3];
    bx::vec3MulMtx(rot, _state._forward.Ptr(), m);
    _state._forward.setElements(rot[0], rot[1], rot[2]);
}

void GameActor::actionJump()
{
    _state._jump = true;
}

// render
void GameActor::draw(bgfx::ProgramHandle program)
{
    // calc mtx
    float mtx[16];
    bx::mtxIdentity(mtx);

    float rot[16];
    bx::mtxRotateY(rot, _horizontalAngle);

    float trans[16];
    bx::mtxTranslate(trans, _state._pos.getX(), _state._pos.getY() - 1.1f, _state._pos.getZ());

    bx::mtxMul(mtx, rot, trans);

    meshSubmit(_mesh, 0, program, mtx);
}

// debugRender
void GameActor::debugDraw()
{
    ddDrawOrb(
            _state._pos.getX(),
            _state._pos.getY(),
            _state._pos.getZ(),
            1.0f
    );
}

void GameActor::update(float dt, const VoxelVolume* volume)
{
    calcPhysic(dt, volume);
}

const ActorState& GameActor::getState() const
{
    return _state;
}

void GameActor::calcPhysic(float dt, const VoxelVolume* volume)
{
    _dy = 0.0f;

    if( _state._jump ) {
        _jy = 12.0f;
        _state._jump = false;
    }

    if( _jy > 0.0f ) {
        _dy = _jy;
        _jy -= 30.0f * dt;
    }

    float vx = _state._forward.getX(), vy = _state._forward.getY(), vz = _state._forward.getZ();

    int estimate = static_cast<int>(roundf(sqrtf(
            powf(_state._forward.getX() * _moveSpeed, 2.0f) +
            powf(_state._forward.getY() * _moveSpeed + fabs(_dy) * 2.0f, 2.0f) +
            powf(_state._forward.getZ() * _moveSpeed, 2.0f)) * dt * 8.0f));

    int step = std::max(8, estimate);

    float ut = dt / step;

    vx = vx * ut * _moveSpeed;
    vy = vy * ut * _moveSpeed;
    vz = vz * ut * _moveSpeed;

    for (int i = 0; i < step; i++) {
        if (_state._flying) {
            _dy = 0;
        }
        else {
            _dy -= ut * 500;
            _dy = std::max(_dy, -900.0f);
        }

        _state._pos.setX( _state._pos.getX() + vx );
        _state._pos.setY( _state._pos.getY() + (vy + _dy * ut) );
        _state._pos.setZ( _state._pos.getZ() + vz );

        float x = _state._pos.getX(), y = _state._pos.getY(), z = _state._pos.getZ();

        int nx = static_cast<int>(roundf(_state._pos.getX()));
        int ny = static_cast<int>(roundf(_state._pos.getY()));
        int nz = static_cast<int>(roundf(_state._pos.getZ()));

        // TODO : Убрать в одтелный метод
        _state._block.setElements(nx, ny, nz);
        _state._chunk.setElements((nx + 16) / 32, (ny + 16)/ 32, (nz + 16)/ 32);

        float px = _state._pos.getX() - nx;
        float py = _state._pos.getY() - ny;
        float pz = _state._pos.getZ() - nz;
        float pad = 0.25;

        const int height = 2;
        int collide = 0;

        for (int dy = 0; dy < height; dy++) {
            if (px < -pad && volume->getVoxel(nx - 1, ny - dy, nz).getMaterial() > 0 ) {
                _state._pos.setX(nx - pad);
            }
            if (px > pad && volume->getVoxel(nx + 1, ny - dy, nz).getMaterial() > 0) {
                _state._pos.setX(nx + pad);
            }
            if (py < -pad && volume->getVoxel(nx, ny - dy - 1, nz).getMaterial() > 0) {
                _state._pos.setY(ny - pad);
                collide = 1;
            }
            if (py > pad && volume->getVoxel(nx, ny - dy + 1, nz).getMaterial() > 0) {
                _state._pos.setY(ny + pad);
                collide = 1;
            }
            if (pz < -pad && volume->getVoxel(nx, ny - dy, nz - 1).getMaterial() > 0) {
                _state._pos.setZ(nz - pad);
            }
            if (pz > pad && volume->getVoxel(nx, ny - dy, nz + 1).getMaterial() > 0) {
                _state._pos.setZ(nz + pad);
            }
        }

        if (collide) {
            _dy = 0;
        }
    }
}

void GameActor::drawActorStateInfo(uint16_t x, uint16_t y, const ActorState& state)
{
    bgfx::dbgTextPrintf(x + 0, y + 1, 0x6f, "ActorState pos: %f %f %f",
                        state._pos.getX(),
                        state._pos.getY(),
                        state._pos.getZ());

    bgfx::dbgTextPrintf(x + 0, y + 2, 0x6f, "ActorState forward: %f %f %f",
                        state._forward.getX(),
                        state._forward.getY(),
                        state._forward.getZ());

    bgfx::dbgTextPrintf(x + 0, y + 3, 0x6f, "ActorState block: %d %d %d",
                        state._block.getX(),
                        state._block.getY(),
                        state._block.getZ());


    bgfx::dbgTextPrintf(x + 0, y + 4, 0x6f, "ActorState chunk: %d %d %d",
                        state._chunk.getX(),
                        state._chunk.getY(),
                        state._chunk.getZ());
}