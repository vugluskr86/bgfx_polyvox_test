//
// Created by vugluskr on 28.08.16.
//

#ifndef BGFX_TEST_GAMESCENE_H
#define BGFX_TEST_GAMESCENE_H

#include <stdint.h>

#include "entry/entry.h"

struct SceneEvent {
    uint32_t _width;
    uint32_t _height;
    uint32_t _debug;
    uint32_t _reset;
    entry::MouseState _mouseState;
    float _deltaTime;

    const bool hasMousePress() const {
        return _mouseState.m_buttons[entry::MouseButton::Right] || _mouseState.m_buttons[entry::MouseButton::Left];
    }

    const bool hasMousePressLeft() const {
        return _mouseState.m_buttons[entry::MouseButton::Left];
    }

    const bool hasMousePressRight() const {
        return _mouseState.m_buttons[entry::MouseButton::Right];
    }
};

class GameScene {
public:
    GameScene() {};
    virtual ~GameScene() {}

    virtual bool handleEvent(const SceneEvent& ev) = 0;
};


#endif //BGFX_TEST_GAMESCENE_H
