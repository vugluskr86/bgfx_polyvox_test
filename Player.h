//
// Created by vugluskr on 27.08.16.
//

#ifndef BGFX_TEST_PLAYER_H
#define BGFX_TEST_PLAYER_H

#include <PolyVox/Vector.h>

struct PlayerState {
   // PolyVox::Vector3DFloat _pos;
    float _moveSpeed;

    PolyVox::Vector3DFloat  _eye;
    PolyVox::Vector3DFloat  _at;
    PolyVox::Vector3DFloat  _up;
    PolyVox::Vector3DFloat  _dir;
    PolyVox::Vector3DFloat  _right;
};

class Player {
    PlayerState _state;

public:
 //   Player();
 //   ~Player();

};


#endif //BGFX_TEST_PLAYER_H
