//
// Created by vugluskr on 28.08.16.
//

#ifndef BGFX_TEST_WORLDGAMESCENE_H
#define BGFX_TEST_WORLDGAMESCENE_H

#include "GameScene.h"

#include "VoxelWorldRender.h"
#include "Player.h"

#include "GameActor.h"

struct MouseCoords {
    int32_t _mx;
    int32_t _my;
};


class MouseStateHandle {
    MouseCoords _mouseNow;
    MouseCoords _mouseLast;

    entry::MouseButton _buttonMask;

    float _dx;
    float _dy;

    void handle(const SceneEvent& ev);
};



class WorldGameScene : public GameScene {

    // TODO : to player stare
    PolyVox::Vector3DInt32 _cubePos;
    PolyVox::Vector3DInt32 _chunkPos;

   // PlayerState _playerState;
    bgfx::UniformHandle _texColor;
    bgfx::TextureHandle _textureColor;
    bgfx::ProgramHandle _program;
    bgfx::ProgramHandle _programMesh;
    VoxelWorldRender* _voxelRender;

    MouseCoords _mouseNow;
    MouseCoords _mouseLast;

    float _horizontalAngle;
    float _verticalAngle;

    float _mouseSpeed;
    float _gamepadSpeed;

    bool _keys[entry::Key::Enum::Count];

    bool _mouseDown;

    float _viewMtx[16];
    float _projMtx[16];

    float _distance;

    void drawDebug();

    std::vector<GameActor*> _actors;

    GameActor* _currentActor;

    VoxelVolume* _VolumeData;
public:
    WorldGameScene();
    virtual ~WorldGameScene();

    void startThreads();

    virtual bool handleEvent(const SceneEvent& ev);
};


#endif //BGFX_TEST_WORLDGAMESCENE_H
