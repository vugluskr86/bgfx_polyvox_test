/*
 * Copyright 2011-2016 Branimir Karadzic. All rights reserved.
 * License: https://github.com/bkaradzic/bgfx#license-bsd-2-clause
 */

#include "common.h"

#include <bx/uint32_t.h>
#include "bgfx_utils.h"

#include "imgui/imgui.h"
#include "debugdraw/debugdraw.h"


#include "GameScene.h"
#include "WorldGameScene.h"

#if BX_PLATFORM_EMSCRIPTEN || BX_PLATFORM_NACL
    static const int64_t highwm = 1000000/35;
    static const int64_t lowwm  = 1000000/27;
#else
    static const int64_t highwm = 1000000 / 65;
    static const int64_t lowwm = 1000000 / 57;
#endif // BX_PLATFORM_EMSCRIPTEN || BX_PLATFORM_NACL


class ExampleCubes : public entry::AppI {
    void init(int _argc, char **_argv) BX_OVERRIDE
    {
        Args args(_argc, _argv);

        srand(3123123);

        _ev._debug = BGFX_DEBUG_TEXT;
        _ev._reset = BGFX_RESET_NONE;
        _ev._width = 1280;
        _ev._height = 720;

        _autoAdjust = true;
        _dim = 16;
        _maxDim = 40;
        _timeOffset = bx::getHPCounter();
        _deltaTimeNs = 0;
        _deltaTimeAvgNs = 0;
        _numFrames = 0;


        bgfx::init(args.m_type, args.m_pciId);
        bgfx::reset(_ev._width, _ev._height, _ev._reset);

        bgfx::setDebug(_ev._debug);

        bgfx::setViewClear(0, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH, 0x303030ff, 1.0f, 0);
        PosNormalColorVertex::init();

        imguiCreate();

        ddInit();

        _scene = new WorldGameScene();
        _scene->startThreads();
    }


    virtual int shutdown() BX_OVERRIDE
    {
        ddShutdown();

        imguiDestroy();

        delete _scene;
        _scene = nullptr;

        bgfx::shutdown();

        return 0;
    }


    bool update() BX_OVERRIDE
    {
        if (!entry::processEvents(_ev._width, _ev._height, _ev._debug, _ev._reset, &_ev._mouseState))
        {
            int64_t now = bx::getHPCounter();
            static int64_t last = now;
            const int64_t hpFreq = bx::getHPFrequency();
            const int64_t frameTime = now - last;
            last = now;
            const double freq = double(hpFreq);
            const double toMs = 1000.0 / freq;

            _ev._deltaTime = float(frameTime / freq);

            _deltaTimeNs += frameTime * 1000000 / hpFreq;

            if (_deltaTimeNs > 1000000) {
                _deltaTimeAvgNs = _deltaTimeNs / bx::int64_max(1, _numFrames);

                if (_autoAdjust) {
                    if (_deltaTimeAvgNs < highwm) {
                        _dim = bx::uint32_min(_dim + 2, _maxDim);
                    }
                    else if (_deltaTimeAvgNs > lowwm) {
                        _dim = bx::uint32_max(_dim - 1, 2);
                    }
                }

                _deltaTimeNs = 0;
                _numFrames = 0;
            }
            else {
                ++_numFrames;
            }

            float time = (float) ((now - _timeOffset) / freq);



            _scene->handleEvent(_ev);

            return true;
        }

        return false;
    }

    void mousePick() {
        /*
        // Set up picking pass
        float pickView[16];
        float pickAt[4]; // Need to inversly project the mouse pointer to determin what we're looking at
        float viewProj[16];
        bx::mtxMul(viewProj, m_viewMtx, m_projMtx);
        float invViewProj[16];
        bx::mtxInverse(invViewProj, viewProj);
        // Mouse coord in NDC
        float mouseXNDC = (m_mouseState.m_mx / (float)m_width) * 2.0f - 1.0f;
        float mouseYNDC = ((m_height - m_mouseState.m_my) / (float)m_height) * 2.0f - 1.0f;
        float mousePosNDC[4] = { mouseXNDC, mouseYNDC, 0, 1.0f };
        // Unproject and perspective divide
        bx::vec4MulMtx(pickAt, mousePosNDC, invViewProj);
        pickAt[3] = 1.0f / pickAt[3];
        pickAt[0] *= pickAt[3];
        pickAt[1] *= pickAt[3];
        pickAt[2] *= pickAt[3];

        const Vector3DFloat v3dStart(this->_pos[0],
                                     this->_pos[1],
                                     this->_pos[2]);

        const Vector3DFloat v3dAt(pickAt[0],
                                  pickAt[1],
                                  pickAt[2]);

        Vector3DFloat vEnd  = v3dAt - v3dStart;
        vEnd.normalise();
        vEnd = vEnd * 100000.0f;

        PickResult resultHit = pickVoxel(&volData, v3dStart, vEnd, VoxelTypes::AIR);

        if( resultHit.didHit ) {
            if( resultHit.previousVoxel.getX() >= 0 && resultHit.previousVoxel.getX() <= 64 &&
                resultHit.previousVoxel.getY() >= 0 && resultHit.previousVoxel.getY() <= 32 &&
                resultHit.previousVoxel.getZ() >= 0 && resultHit.previousVoxel.getZ() <= 64) {
                if( m_clickBtn == entry::MouseButton::Left ) {
                    volData.setVoxel(resultHit.previousVoxel, VoxelTypes::GRASS);
                    m_voxelDirty = true;
                } else {
                    volData.setVoxel(resultHit.hitVoxel, VoxelTypes::AIR);
                    m_voxelDirty = true;
                }
            }
        }
        */
    }



    WorldGameScene* _scene;
    SceneEvent _ev;

    // статистика
    bool _autoAdjust;
    int32_t _dim;
    int32_t _maxDim;
    int64_t _timeOffset;
    int64_t _deltaTimeNs;
    int64_t _deltaTimeAvgNs;
    int64_t _numFrames;
};

ENTRY_IMPLEMENT_MAIN(ExampleCubes);
