//
// Created by vugluskr on 20.08.16.
//

#include "ChunkMesh.h"

bgfx::VertexDecl PosNormalColorVertex::ms_decl;

ChunkMesh::ChunkMesh()
        : _dirty(true) {
    m_vbh.idx = bgfx::invalidHandle;
    m_ibh.idx = bgfx::invalidHandle;

}

ChunkMesh::~ChunkMesh() {
    if (bgfx::isValid(m_vbh) )
    {
        bgfx::destroyVertexBuffer(m_vbh);
    }

    if (bgfx::isValid(m_ibh) )
    {
        bgfx::destroyIndexBuffer(m_ibh);
    }
    reset();
}

void ChunkMesh::reset() {
    if (bgfx::isValid(m_vbh) )
    {
        bgfx::destroyVertexBuffer(m_vbh);
    }

    if (bgfx::isValid(m_ibh) )
    {
        bgfx::destroyIndexBuffer(m_ibh);
    }
    m_vbh.idx = bgfx::invalidHandle;
    m_ibh.idx = bgfx::invalidHandle;
    _dirty = true;
}
/*
void ChunkMesh::destroy() {
    if (bgfx::isValid(m_vbh) )
    {
        bgfx::destroyVertexBuffer(m_vbh);
    }

    if (bgfx::isValid(m_ibh) )
    {
        bgfx::destroyIndexBuffer(m_ibh);
    }

    m_created = false;
}
*/
void ChunkMesh::update() {
    if( _dirty ) {
        // Create static vertex buffer.
        m_vbh = bgfx::createVertexBuffer(
                // Static data can be passed with bgfx::makeRef
                bgfx::makeRef(m_vertex.data(), static_cast<uint32_t>(m_vertex.size() * sizeof(PosNormalColorVertex)) )
                , PosNormalColorVertex::ms_decl
        );

        // Create static index buffer.
        m_ibh = bgfx::createIndexBuffer(
                // Static data can be passed with bgfx::makeRef
                bgfx::makeRef(m_indices.data(), static_cast<uint32_t>(m_indices.size() * sizeof(uint16_t)) )
        );

        _dirty = false;
    }
}

void ChunkMesh::draw(bgfx::ProgramHandle program) {

  //  update();

    if( !_dirty ) {
        float mtx[16];
        bx::mtxIdentity(mtx);
        bx::mtxTranslate(mtx, m_origin.getX(), m_origin.getY(), m_origin.getZ());

        bgfx::setTransform(mtx);

        bgfx::setVertexBuffer(m_vbh);
        bgfx::setIndexBuffer(m_ibh);

        bgfx::setState(BGFX_STATE_RGB_WRITE | BGFX_STATE_ALPHA_WRITE
                       | BGFX_STATE_DEPTH_TEST_LESS
                       | BGFX_STATE_DEPTH_WRITE
                       | BGFX_STATE_CULL_CCW
                       | BGFX_STATE_MSAA );

        bgfx::submit(0, program);
    }
}
