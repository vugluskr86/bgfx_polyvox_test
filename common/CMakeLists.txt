cmake_minimum_required(VERSION 3.5)
project(common)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

include_directories(libs)

set(SOURCE_FILES
        aviwriter.h
        bgfx_utils.cpp
        bgfx_utils.h
        bounds.cpp
        bounds.h
        camera.cpp
        camera.h
        common.h
        cube_atlas.cpp
        cube_atlas.h
        packrect.h
        debugdraw/debugdraw.cpp
        debugdraw/debugdraw.h
        debugdraw/fs_debugdraw_fill.bin.h
        debugdraw/fs_debugdraw_fill_lit.bin.h
        debugdraw/fs_debugdraw_lines.bin.h
        debugdraw/fs_debugdraw_lines_stipple.bin.h
        debugdraw/vs_debugdraw_fill.bin.h
        debugdraw/vs_debugdraw_fill_lit.bin.h
        debugdraw/vs_debugdraw_lines.bin.h
        debugdraw/vs_debugdraw_lines_stipple.bin.h
        entry/cmd.cpp
        entry/cmd.h
        entry/dbg.cpp
        entry/dbg.h
        entry/entry_android.cpp
        entry/entry_asmjs.cpp
        entry/entry.cpp
        entry/entry_glfw.cpp
        entry/entry.h
        entry/entry_nacl.cpp
        entry/entry_noop.cpp
        entry/entry_p.h
        entry/entry_sdl.cpp
        entry/entry_windows.cpp
        entry/entry_x11.cpp
        entry/input.cpp
        entry/input.h
        font/font_manager.cpp
        font/font_manager.h
        font/fs_font_basic.bin.h
        font/fs_font_distance_field.bin.h
        font/fs_font_distance_field_subpixel.bin.h
        font/text_buffer_manager.cpp
        font/text_buffer_manager.h
        font/text_metrics.cpp
        font/text_metrics.h
        font/utf8.cpp
        font/utf8.h
        font/vs_font_basic.bin.h
        font/vs_font_distance_field.bin.h
        font/vs_font_distance_field_subpixel.bin.h
        imgui/droidsans.ttf.h
        imgui/fs_imgui_color.bin.h
        imgui/fs_imgui_cubemap.bin.h
        imgui/fs_imgui_image.bin.h
        imgui/fs_imgui_image_swizz.bin.h
        imgui/fs_imgui_latlong.bin.h
        imgui/fs_imgui_texture.bin.h
        imgui/fs_ocornut_imgui.bin.h
        imgui/icons_font_awesome.ttf.h
        imgui/icons_kenney.ttf.h
        imgui/imgui.cpp
        imgui/imgui.h
        imgui/ocornut_imgui.cpp
        imgui/ocornut_imgui.h
        imgui/robotomono_regular.ttf.h
        imgui/roboto_regular.ttf.h
        imgui/scintilla.cpp
        imgui/scintilla.h
        imgui/vs_imgui_color.bin.h
        imgui/vs_imgui_cubemap.bin.h
        imgui/vs_imgui_image.bin.h
        imgui/vs_imgui_latlong.bin.h
        imgui/vs_imgui_texture.bin.h
        imgui/vs_ocornut_imgui.bin.h
        nanovg/fontstash.h
        nanovg/fs_nanovg_fill.bin.h
        nanovg/nanovg_bgfx.cpp
        nanovg/nanovg.cpp
        nanovg/nanovg.h
        nanovg/vs_nanovg_fill.bin.h
        )

add_library(common STATIC ${SOURCE_FILES})


#target_link_libraries(common ib-compress ocornut-imgui remotery stb)