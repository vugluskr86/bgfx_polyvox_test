cmake_minimum_required(VERSION 3.5)
project(imgui)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

include_directories(../libs)

set(SOURCE_FILES
    droidsans.ttf.h
    fs_imgui_color.bin.h
    fs_imgui_cubemap.bin.h
    fs_imgui_image.bin.h
    fs_imgui_image_swizz.bin.h
    fs_imgui_latlong.bin.h
    fs_imgui_texture.bin.h
    fs_ocornut_imgui.bin.h
    icons_font_awesome.ttf.h
    icons_kenney.ttf.h
    imgui.cpp
    imgui.h
    ocornut_imgui.cpp
    ocornut_imgui.h
    robotomono_regular.ttf.h
    roboto_regular.ttf.h
    scintilla.cpp
    scintilla.h
    vs_imgui_color.bin.h
    vs_imgui_cubemap.bin.h
    vs_imgui_image.bin.h
    vs_imgui_latlong.bin.h
    vs_imgui_texture.bin.h
    vs_ocornut_imgui.bin.h
        )

add_library(imgui STATIC ${SOURCE_FILES})