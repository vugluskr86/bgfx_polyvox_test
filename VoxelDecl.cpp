//
// Created by vugluskr on 20.08.16.
//

#include "VoxelDecl.h"

/*
bool operator==(const VoxelDecl& left, const VoxelDecl& right) {
    return left.m_type == right.m_type;
}

bool operator!=(const VoxelDecl& left, const VoxelDecl& right) {
    return left.m_type != right.m_type;
}

bool isVoxelTransparentFunction(VoxelDecl voxel)
{
    return voxel.isTransparent();
}
*/

uint8_t vertexAO(uint8_t side1, uint8_t side2, uint8_t corner) {
    if(side1 && side2) {
        return 0;
    }
    return 3 - (side1 + side2 + corner);
}

void updateAmbient(PolyVox::RawVolume<VoxelDecl>* volData, const PolyVox::Region& region)
{/*
    for (int32_t z = region.getLowerZ(); z <= region.getUpperZ(); z++) {
        uint32_t regZ = z - region.getLowerZ();
        for (int32_t y = region.getLowerY(); y <= region.getUpperY(); y++) {
            uint32_t regY = y - region.getLowerY();
            for (int32_t x = region.getLowerX(); x <= region.getUpperX(); x++) {
                uint32_t regX = x - region.getLowerX();

                auto v = volData->getVoxel(regX, regY, regZ);

                if( !v.isTransparent() ) {

                    v.m_ao[1] = 0;
                    v.m_ao[0] = 0;
                    v.m_ao[2] = 0;
                    v.m_ao[3] = 0;

                    v.m_ao[3] = vertexAO(
                            volData->getVoxel(regX,     regY + 1, regZ + 1) != VoxelTypes::AIR ? 1 : 0,
                            volData->getVoxel(regX - 1, regY + 1, regZ    ) != VoxelTypes::AIR ? 1 : 0,
                            volData->getVoxel(regX - 1, regY + 1, regZ + 1) != VoxelTypes::AIR ? 1 : 0
                    );

                    v.m_ao[0] = vertexAO(
                            volData->getVoxel(regX - 1, regY + 1, regZ    ) != VoxelTypes::AIR ? 1 : 0,
                            volData->getVoxel(regX,     regY + 1, regZ - 1) != VoxelTypes::AIR ? 1 : 0,
                            volData->getVoxel(regX - 1, regY + 1, regZ - 1) != VoxelTypes::AIR ? 1 : 0
                    );

                    v.m_ao[1] = vertexAO(
                            volData->getVoxel(regX,     regY + 1, regZ - 1) != VoxelTypes::AIR ? 1 : 0,
                            volData->getVoxel(regX + 1, regY + 1, regZ    ) != VoxelTypes::AIR ? 1 : 0,
                            volData->getVoxel(regX + 1, regY + 1, regZ - 1) != VoxelTypes::AIR ? 1 : 0
                    );

                    v.m_ao[2] = vertexAO(
                            volData->getVoxel(regX + 1, regY + 1, regZ    ) != VoxelTypes::AIR ? 1 : 0,
                            volData->getVoxel(regX,     regY + 1, regZ + 1) != VoxelTypes::AIR ? 1 : 0,
                            volData->getVoxel(regX + 1, regY + 1, regZ + 1) != VoxelTypes::AIR ? 1 : 0
                    );

                    volData->setVoxel(regX, regY, regZ, v);
                }
            }
        }
    }
*/}

/*
PolyVox::Vertex<uint32_t> decodeVoxelVertex(const PolyVox::CubicVertex<VoxelDecl>& cubicVertex)
{
    PolyVox::Vertex<uint32_t> result;
    result.position = decodePosition(cubicVertex.encodedPosition);

    result.normal.setElements(0.0f, 0.0f, 0.0f); // Currently not calculated

    VoxelDecl v = cubicVertex.data;

    ABGR vc = ABGR(v);

    float tint = 1.0f;

    switch (cubicVertex.face) {
        case PolyVox::FaceNames::NegativeX:
            result.normal.setElements(-1.0f, 0.0f, 0.0f);
            break;

        case PolyVox::FaceNames::PositiveX:
            result.normal.setElements(1.0f, 0.0f, 0.0f);
            break;

        case PolyVox::FaceNames::NegativeY:
            result.normal.setElements(0.0f, -1.0f, 0.0f);
            break;

        case PolyVox::FaceNames::PositiveY:
      //      tint -= 1.0f - (v.m_ao[cubicVertex.vnum] / 3.0f);
            result.normal.setElements(0.0f, 1.0f, 0.0f);
            break;

        case PolyVox::FaceNames::NegativeZ:
            result.normal.setElements(0.0f, 0.0f, -1.0f);
            break;

        case PolyVox::FaceNames::PositiveZ:
            result.normal.setElements(0.0f, 0.0f, 1.0f);
            break;
    }

    vc.green *= tint;
    vc.blue  *= tint;
    vc.red   *= tint;

    result.data = vc.raw; // Data is not encoded

    return result;
}
*/