//
// Created by vugluskr on 28.08.16.
//

#ifndef BGFX_TEST_GAMEACTOR_H
#define BGFX_TEST_GAMEACTOR_H

#include <bx/uint32_t.h>
#include "bgfx_utils.h"
#include "VoxelDecl.h"

#include <PolyVox/Vector.h>

#include "common.h"
#include "bgfx_utils.h"

class WorldGameScene;

struct ActorState {
    float _moveSpeed;
    float _flySpeed;

    // btTransform _trans;

    PolyVox::Vector3DFloat _forward;
    PolyVox::Vector3DFloat _pos;
    PolyVox::Vector3DInt32 _block;
    PolyVox::Vector3DInt32 _chunk;

    bool _flying;
    bool _jump;

    ActorState() {
        reset();
    }

    inline void reset() {
        _moveSpeed = 3.0f;
        _flySpeed = 0.5f;

        _forward = PolyVox::Vector3DFloat(0, 0, 1);
        _pos = PolyVox::Vector3DFloat(0, 50, 0);
        _block = PolyVox::Vector3DInt32(0, 0, 0);
        _chunk = PolyVox::Vector3DInt32(0, 0, 0);
        _flying = false;
        _jump = false;
    }
};

class GameActor {
    Mesh* _mesh;
    ActorState _state;

    float _dy;
    float _jy;
    float _verticalAngle;
    float _horizontalAngle;
    float _moveSpeed;

    void calcPhysic(float dt, const VoxelVolume* volume);
public:
    GameActor(ActorState &_state);
    virtual ~GameActor();

    // input
    void actionMoveForward();
    void actionMoveBackward();
    void actionMoveStop();
    void actionRotateY(float angle);
    void actionJump();

    // render
    void draw(bgfx::ProgramHandle program);
    // debugRender
    void debugDraw();

    void update(float dt, const VoxelVolume* volume);

    const ActorState& getState() const;


    static void drawActorStateInfo(uint16_t x, uint16_t y, const ActorState& state);
};


#endif //BGFX_TEST_GAMEACTOR_H
