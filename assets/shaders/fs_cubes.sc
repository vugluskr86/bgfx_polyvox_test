$input v_pos, v_light, v_normal, v_color0

/*
   https://gist.github.com/patriciogonzalezvivo/20263fe85d52705e4530
*/

#include "../../common/common.sh"

SAMPLER2D(s_texColor,  0);

vec3 getTriPlanarBlend(vec3 _wNorm){
   // in wNorm is the world-space normal of the fragment
   vec3 blending = abs( _wNorm );
   blending = normalize(max(blending, 0.00001)); // Force weights to sum to 1.0
   float b = (blending.x + blending.y + blending.z);
   blending /= vec3(b, b, b);
   return blending;
}

void main()
{
	vec3 normal = normalize(cross(dFdy(v_pos.xyz), dFdx(v_pos.xyz)));

   vec4 sampleX = texture2D(s_texColor, v_pos.yz); // Project along x axis
   vec4 sampleY = texture2D(s_texColor, v_pos.xz); // Project along y axis
   vec4 sampleZ = texture2D(s_texColor, v_pos.xy); // Project along z axis

   vec3 absNormal = getTriPlanarBlend(normal);

   gl_FragColor = sampleX * absNormal.x + sampleY * absNormal.y + sampleZ * absNormal.z;

   //gl_FragColor = vec4(abs(normal) * 0.5 + vec3(0.5, 0.5, 0.5), 1.0);
}
