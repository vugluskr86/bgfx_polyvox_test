$input a_position, a_normal, a_color0
$output v_pos, v_light, v_normal, v_color0

/*
default exports

uniform vec4  u_viewRect;
uniform vec4  u_viewTexel;
uniform mat4  u_view;
uniform mat4  u_invView;
uniform mat4  u_proj;
uniform mat4  u_invProj;
uniform mat4  u_viewProj;
uniform mat4  u_invViewProj;
uniform mat4  u_model[BGFX_CONFIG_MAX_BONES];
uniform mat4  u_modelView;
uniform mat4  u_modelViewProj;
uniform vec4  u_alphaRef4;
*/

#include "../../common/common.sh"

void main()
{
	vec3 pos    = a_position;

	v_pos = mul(u_model[0], vec4(pos, 1.0) ).xyz;

	vec3 LightPosition_worldspace    = vec3(15.0,32.0,15.0);
	vec3 vertexPosition_cameraspace  = ( u_modelView * vec4(pos,1) ).xyz;
	vec3 EyeDirection_cameraspace    = vec3(0.0,0.0,0.0) - vertexPosition_cameraspace;
	vec3 LightPosition_cameraspace   = ( u_view * vec4(LightPosition_worldspace,1)).xyz;
	vec3 LightDirection_cameraspace  = LightPosition_cameraspace + EyeDirection_cameraspace;

	v_normal = (u_modelView * vec4(a_normal,0)).xyz;

	v_light = LightDirection_cameraspace;
	v_color0 = a_color0;

	gl_Position = mul(u_modelViewProj, vec4(pos, 1.0) );

}
