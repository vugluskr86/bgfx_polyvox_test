//
// Created by vugluskr on 28.08.16.
//

#include "WorldGameScene.h"

#include "common.h"

#include <bx/uint32_t.h>
#include "bgfx_utils.h"

#include "camera.h"
#include "imgui/imgui.h"
#include "debugdraw/debugdraw.h"
#include "entry/input.h"
#include "entry/cmd.h"


const float BALL_RADIUS = 2.0f;

/*
#define WORLD_CAMERA_KEY_FORWARD   UINT8_C(0x01)
#define WORLD_CAMERA_KEY_BACKWARD  UINT8_C(0x02)
#define WORLD_CAMERA_KEY_LEFT      UINT8_C(0x04)
#define WORLD_CAMERA_KEY_RIGHT     UINT8_C(0x08)
#define WORLD_CAMERA_KEY_UP        UINT8_C(0x10)
#define WORLD_CAMERA_KEY_DOWN      UINT8_C(0x20)
*/

static void cmd(const void *_userData)
{
    //cmdExec((const char *) _userData);
}

static const InputBinding s_camBindings[] =
{
    {entry::Key::KeyW, entry::Modifier::None, 0, cmd, "move forward"},
    {entry::Key::GamepadUp, entry::Modifier::None, 0, cmd, "move forward"},
    {entry::Key::KeyA, entry::Modifier::None, 0, cmd, "move left"},
    {entry::Key::GamepadLeft, entry::Modifier::None, 0, cmd, "move left"},
    {entry::Key::KeyS, entry::Modifier::None, 0, cmd, "move backward"},
    {entry::Key::GamepadDown, entry::Modifier::None, 0, cmd, "move backward"},
    {entry::Key::KeyD, entry::Modifier::None, 0, cmd, "move right"},
    {entry::Key::GamepadRight, entry::Modifier::None, 0, cmd, "move right"},
    {entry::Key::KeyQ, entry::Modifier::None, 0, cmd, "move down"},
    {entry::Key::GamepadShoulderL, entry::Modifier::None, 0, cmd, "move down"},
    {entry::Key::KeyE, entry::Modifier::None, 0, cmd, "move up"},
    {entry::Key::GamepadShoulderR, entry::Modifier::None, 0, cmd, "move up"},
    INPUT_BINDING_END
};


#include "Perlin.h"
#include <ctime>

class PerlinNoisePager : public PolyVox::PagedVolume<PolyVox::MaterialDensityPair44>::Pager
{
public:
    /// Constructor
    PerlinNoisePager()
            : PolyVox::PagedVolume<PolyVox::MaterialDensityPair44>::Pager()
    {
    }

    /// Destructor
    virtual ~PerlinNoisePager() {};

    virtual void pageIn(const PolyVox::Region& region, PolyVox::PagedVolume<PolyVox::MaterialDensityPair44>::Chunk* pChunk)
    {
        Perlin perlin(2, 2, 1, 2312387123);

        for (int x = region.getLowerX(); x <= region.getUpperX(); x++)
        {
            for (int z = region.getLowerZ(); z <= region.getUpperZ(); z++)
            {
                float perlinVal = perlin.Get(x / static_cast<float>(255 - 1), z / static_cast<float>(255 - 1));

                perlinVal *= 32;
                for (int y = region.getLowerY(); y <= region.getUpperY(); y++)
                {
                    PolyVox::MaterialDensityPair44 voxel;
                    if (y < perlinVal)
                    {
                        const int xpos = 10;
                        const int zpos = 30;
                        if ((x - xpos)*(x - xpos) + (z - zpos)*(z - zpos) < 100)
                        {
                            // tunnel
                            voxel.setMaterial(0);
                            voxel.setDensity(PolyVox::MaterialDensityPair44::getMinDensity());
                        }
                        else
                        {




                            // solid
                            voxel.setMaterial(245);
                            voxel.setDensity(PolyVox::MaterialDensityPair44::getMaxDensity());
                        }
                    }
                    else
                    {
                        voxel.setMaterial(0);
                        voxel.setDensity(PolyVox::MaterialDensityPair44::getMinDensity());
                    }

                    // Voxel position within a chunk always start from zero. So if a chunk represents region (4, 8, 12) to (11, 19, 15)
                    // then the valid chunk voxels are from (0, 0, 0) to (7, 11, 3). Hence we subtract the lower corner position of the
                    // region from the volume space position in order to get the chunk space position.
                    pChunk->setVoxel(x - region.getLowerX(), y - region.getLowerY(), z - region.getLowerZ(), voxel);
                }
            }
        }
    }

    virtual void pageOut(const PolyVox::Region& region, PolyVox::PagedVolume<PolyVox::MaterialDensityPair44>::Chunk* /*pChunk*/)
    {
        std::cout << "warning unloading region: " << region.getLowerCorner() << " -> " << region.getUpperCorner() << std::endl;
    }
};


WorldGameScene::WorldGameScene() :
        GameScene()
{
    _mouseNow._mx = 0;
    _mouseNow._my = 0;
    _mouseLast._mx = 0;
    _mouseLast._my = 0;

    _horizontalAngle = 90.0f;
    _verticalAngle = -25.0f;

    _mouseSpeed = 0.020f;
    _gamepadSpeed = 0.04f;
    //   _moveSpeed = 30.0f;

    PerlinNoisePager* pager = new PerlinNoisePager();
    _VolumeData = new VoxelVolume(pager, 128 * 1024 * 1024, 64);

    _voxelRender = new VoxelWorldRender(_VolumeData);

    //  _keys = 0;
    _mouseDown = false;

  //  _playerState._pos = PolyVox::Vector3DFloat(0,0,0);

  //  _movement = false;

 //   _playerState._moveSpeed = 0.05f;

 //   _playerState._eye = PolyVox::Vector3DFloat(0,0,0);
 //   _playerState._at = PolyVox::Vector3DFloat(0,0,-1);
 //   _playerState._up = PolyVox::Vector3DFloat(0,1,0);
 //   _playerState._dir = PolyVox::Vector3DFloat(1,0,0);
 //   _playerState._right = PolyVox::Vector3DFloat(0,0,1);

    _distance = 10.0f;

    PosNormalColorVertex::init();

   // update(0.0f, mouseState);

    // Create program from shaders.
    _program = loadProgram("vs_cubes", "fs_cubes");
    _programMesh = loadProgram("vs_mesh", "fs_mesh");

    _texColor = bgfx::createUniform("s_texColor",  bgfx::UniformType::Int1);
    _textureColor = loadTexture("textures/stone.png");

    inputAddBindings("camBindings", s_camBindings);

    ActorState _initState;
    _initState.reset();
    _currentActor = new GameActor(_initState);
    _actors.push_back(_currentActor);
}

WorldGameScene::~WorldGameScene()
{
    inputRemoveBindings("camBindings");

    bgfx::destroyProgram(_program);
    bgfx::destroyProgram(_programMesh);
}


void WorldGameScene::startThreads()
{
    _voxelRender->start();
}

bool WorldGameScene::handleEvent(const SceneEvent& ev) {

    // вперд
    if (!_keys[entry::Key::KeyW] && inputGetKeyState(entry::Key::KeyW)) {
        _currentActor->actionMoveForward(); // TODO : Через флаг
        _keys[entry::Key::KeyW] = true;
    }

    // назад
    if (!_keys[entry::Key::KeyS] && inputGetKeyState(entry::Key::KeyS)) {
        _currentActor->actionMoveBackward(); // TODO : Через флаг
        _keys[entry::Key::KeyS] = true;
    }

    if(_keys[entry::Key::KeyW] && !inputGetKeyState(entry::Key::KeyW)) {
        _currentActor->actionMoveStop();
        _keys[entry::Key::KeyW] = false;
    }

    if(_keys[entry::Key::KeyS] && !inputGetKeyState(entry::Key::KeyS)) {
        _currentActor->actionMoveStop();
        _keys[entry::Key::KeyS] = false;
    }

    if(!_keys[entry::Key::Space] && inputGetKeyState(entry::Key::Space)) {
        _currentActor->actionJump();
        _keys[entry::Key::Space] = true;
    }
    if(_keys[entry::Key::Space] && !inputGetKeyState(entry::Key::Space)) {
        _keys[entry::Key::Space] = false;
    }

    // поворот
    if (inputGetKeyState(entry::Key::KeyD)) {
        _currentActor->actionRotateY(-0.01f); // TODO : Через флаг
    }
    // поворот
    if (inputGetKeyState(entry::Key::KeyA)) {
        _currentActor->actionRotateY(0.01f); // TODO : Через флаг
    }


    for(auto act : _actors) act->update(ev._deltaTime, _VolumeData);

    if (!_mouseDown) {
        _mouseLast._mx = ev._mouseState.m_mx;
        _mouseLast._my = ev._mouseState.m_my;
    }

    _mouseDown = !!ev._mouseState.m_buttons[entry::MouseButton::Right];

    if (_mouseDown) {
        _mouseNow._mx = ev._mouseState.m_mx;
        _mouseNow._my = ev._mouseState.m_my;
    }

    if (_mouseDown) {
        int32_t deltaX = _mouseNow._mx - _mouseLast._mx;
        int32_t deltaY = _mouseNow._my - _mouseLast._my;

        _horizontalAngle += _mouseSpeed * float(deltaX);
        _verticalAngle -= _mouseSpeed * float(deltaY);

        _mouseLast._mx = _mouseNow._mx;
        _mouseLast._my = _mouseNow._my;
    }

    entry::GamepadHandle handle = {0};

    _horizontalAngle += _gamepadSpeed * inputGetGamepadAxis(handle, entry::GamepadAxis::RightX) / 32768.0f;
    _verticalAngle   -= _gamepadSpeed * inputGetGamepadAxis(handle, entry::GamepadAxis::RightY) / 32768.0f;

    imguiBeginFrame(ev._mouseState.m_mx, ev._mouseState.m_my,
        (ev._mouseState.m_buttons[entry::MouseButton::Left] ? IMGUI_MBUT_LEFT : 0)
        | (ev._mouseState.m_buttons[entry::MouseButton::Right] ? IMGUI_MBUT_RIGHT : 0)
        | (ev._mouseState.m_buttons[entry::MouseButton::Middle] ? IMGUI_MBUT_MIDDLE : 0),
        ev._mouseState.m_mz, ev._width, ev._height
    );

    const ActorState& state = _currentActor->getState();

    imguiEndFrame();

    bgfx::dbgTextClear();
    drawDebug();

    GameActor::drawActorStateInfo(0, 10, state);

    // _actors

    _voxelRender->update(state._block);

    // Set view 0 default viewport.
    bgfx::setViewRect(0, 0, 0, uint16_t(ev._width), uint16_t(ev._height));

    //  FreeCamera::cameraGetViewMtx(&_cam.getViewMatrix()[0][0]);
    bx::mtxProj(_projMtx, 60.0f, float(ev._width) / float(ev._height), 0.1f, 20000.0f);

    if( ev._mouseState.m_mz ) {
        _distance = -ev._mouseState.m_mz * 0.5f;
    }

    //if( _distance <= 2.0f ) _distance = 2.0f;
    //if( _distance >= 20.0f ) _distance = 20.0f;

    PolyVox::Vector3DFloat offsetVector = state._pos + PolyVox::Vector3DFloat(
           (_distance * -sinf(_horizontalAngle*(M_PI/180.0f)) * cosf((_verticalAngle)*(M_PI/180.0f))),
           (_distance * -sinf((_verticalAngle)*(M_PI/180.0f))),
           (-_distance * cosf((_horizontalAngle)*(M_PI/180.0f)) * cosf((_verticalAngle)*(M_PI/180.0f)))
    );

    PolyVox::Vector3DFloat _eye(state._pos);

    const PolyVox::Vector3DFloat vup(0, 1, 0);

    bx::mtxLookAt(_viewMtx, offsetVector.Ptr(), _eye.Ptr(), vup.Ptr());

    bgfx::setViewTransform(0, _viewMtx, _projMtx);

    bgfx::touch(0);

    bgfx::setTexture(0, _texColor,  _textureColor, BGFX_TEXTURE_MIN_POINT | BGFX_TEXTURE_MAG_POINT );

    _voxelRender->render(_program);

    for(auto act : _actors) act->draw(_programMesh);

    bgfx::frame();

    return true;
}

void WorldGameScene::drawDebug()
{
    ddBegin(0);

    ddDrawAxis(0, 0, 0, 100);
    float _gridN[3] = {0, 1, 0};
    float _gridOrigin[3] = {0.5f, -0.5f, 0.5f};
    ddDrawGrid(_gridN, _gridOrigin, 100);

    for(auto act : _actors) act->debugDraw();

    _voxelRender->renderDebug();

    ddEnd();
    bgfx::dbgTextPrintf(0, 1, 0x4f, "DEBUG");
    _voxelRender->renderInfo(8);
}